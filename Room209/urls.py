from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
import Room209.settings as settings
from rest_framework import routers
from R209.views import Blah, Index, login_user, logout_user
from downloads.views import Downloads, new_file
from showcase.views import ProjectShowcase, LanguageShowcase, EventShowcase
from urlShortner import views


router = routers.DefaultRouter()

urlpatterns = patterns('',
                       # Admin Site
                       url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
                       url(r'^7h!W/', include(admin.site.urls)),

                       # Main Site
                       url(r'^$', Index.as_view(), name='Index'),
                       url(r'^home/$', Index.as_view(), name='Index'),
                       url(r'^downloads/(?P<category>\w{0,50})$', Downloads.as_view(search=None), name='Downloads'),
                       url(r'^downloads/upload/$', new_file, name='New File'),
                       url(r'^showcase$', ProjectShowcase.as_view(), name='ProjectShowcase'),
                       url(r'^language/(?P<language>\w{0,50})$', LanguageShowcase.as_view(), name='LanguageShowcase'),
                       url(r'^event/(?P<event>\w{0,50})$', EventShowcase.as_view(), name='EventShowcase'),

                       # Accounts
                       url(r'^accounts/login/$', login_user),
                       url(r'^accounts/logout/$', logout_user),

                       # API
                       url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
                       url(r'^Dn2@/', include(router.urls)),

                       # Other
                       url(r'Bah/$', Blah.as_view(), name='Test'),

                       # URLs
                       url(r'(?P<short>\w{0,50})/$', views.url, name='URLShortner'),
                       url(r'url$', views.url_create),
                       ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

