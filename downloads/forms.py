from django import forms
from downloads.models import Category, FileItem


class FileForm(forms.ModelForm):

    class Meta:
        model = FileItem

