# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=10, verbose_name=b'Name')),
                ('slug', models.SlugField(max_length=10, verbose_name=b'Slug')),
            ],
            options={
                'verbose_name': 'Category',
                'verbose_name_plural': 'Categories',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FileItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=20, verbose_name=b'Name')),
                ('description', models.TextField(default=b'Welp, I was too lazy to put a description just google it', verbose_name=b'Description')),
                ('item', models.FileField(upload_to=b'files', verbose_name=b'Item')),
                ('icon', models.ImageField(upload_to=b'', verbose_name=b'Icon')),
                ('File Type', models.ForeignKey(related_name='files', verbose_name=b'Category', to='downloads.Category')),
            ],
            options={
                'verbose_name': 'File',
                'verbose_name_plural': 'Files',
            },
            bases=(models.Model,),
        ),
    ]
