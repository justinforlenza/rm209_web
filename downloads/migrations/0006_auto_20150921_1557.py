# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('downloads', '0005_auto_20150921_1556'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fileitem',
            name='icon',
            field=models.ImageField(upload_to=b'icons', null=True, verbose_name=b'Icon', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='fileitem',
            name='item',
            field=models.FileField(upload_to=b'files', null=True, verbose_name=b'Item', blank=True),
            preserve_default=True,
        ),
    ]
