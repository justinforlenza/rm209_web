# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('downloads', '0003_auto_20150811_0247'),
    ]

    operations = [
        migrations.RenameField(
            model_name='fileitem',
            old_name='File Type',
            new_name='FileType',
        ),
    ]
