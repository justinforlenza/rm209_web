# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('downloads', '0002_auto_20150811_0231'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fileitem',
            name='name',
            field=models.CharField(max_length=50, verbose_name=b'Name'),
            preserve_default=True,
        ),
    ]
