# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('downloads', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='fileitem',
            name='icon',
            field=models.ImageField(upload_to=b'icons', verbose_name=b'Icon'),
            preserve_default=True,
        ),
    ]
