from django.template import Library
register = Library()


@register.filter
def get_range(value):
    blah = []
    for i in range(value):
        blah.append(i+1)
    return blah


@register.filter
def remove_plus(value):
    value = value.replace('+', ' ')
    return value
