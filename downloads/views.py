from django.views.generic import ListView
from downloads.models import FileItem
from downloads.forms import FileForm
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required


class Downloads(ListView):
    search = None
    template_name = '../templates/downloads.html'
    model = FileItem
    context_object_name = 'files'
    paginate_by = 5

    def get_queryset(self):
        q = self.request.GET.get('q')
        qs = super(Downloads, self).get_queryset()
        if self.kwargs['category']:
            qs = qs.filter(FileType__slug=self.kwargs['category'])

        if q:
            name = qs.filter(name__icontains=q)
            description = qs.filter(description__icontains=q)
            qs = name | description

        return qs.order_by('name')

    def get_context_data(self, **kwargs):
        context = super(Downloads, self).get_context_data(**kwargs)
        if self.request.GET.get('q'):
            context['search'] = self.request.GET.get('q')
        return context


@login_required
def new_file(request):
    if request.method == "POST":
        form = FileForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('/downloads/')
    else:
        form = FileForm()
    return render(request, '../templates/file_upload.html', {'form': form})
