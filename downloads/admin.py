from django.contrib import admin
from downloads.models import Category, FileItem


class CategoryAdmin(admin.ModelAdmin):
    list_display = ['name', 'slug']


class FileItemAdmin(admin.ModelAdmin):
    list_display = ['name', 'item', 'icon']
    list_per_page = 10
    ordering = ['name']
    search_fields = ['name', 'category']

admin.site.register(Category, CategoryAdmin)
admin.site.register(FileItem, FileItemAdmin)