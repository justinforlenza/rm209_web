from django.db import models
from django.db.models import *
fileDescription = 'Welp, I was too lazy to put a description just google it'


class Category(models.Model):
    name = CharField('Name', max_length=10, blank=False)
    slug = SlugField('Slug', max_length=10, blank=False)

    class Meta(object):
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class FileItem(models.Model):
    name = CharField('Name', max_length=50, blank=False)
    description = TextField('Description', default=fileDescription)
    category = ForeignKey(Category, name='FileType', blank=False, related_name='files', verbose_name='Category')
    item = FileField('Item', upload_to='files', blank=True, null=True)
    icon = ImageField('Icon', upload_to='icons', null=True, blank=True)

    class Meta(object):
        verbose_name = 'File'
        verbose_name_plural = 'Files'

    def __str__(self):
        return self.name
