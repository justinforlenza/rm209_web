from django.db import models
import datetime

YEAR_CHOICES = []
for r in range(2017, (datetime.datetime.now().year+1)):
    YEAR_CHOICES.append((r, r))


class Panel(models.Model):
    identifier = models.CharField(max_length=1, blank=False)
    name = models.CharField(max_length=10, blank=False)
    icon = models.CharField(max_length=25, blank=False)
    description = models.TextField(blank=False)
    link = models.CharField(blank=False, max_length=40)

    def __str__(self):
        return self.name


class Student(models.Model):
    name = models.CharField('Name', max_length=100, blank=False)
    grad_year = models.IntegerField('Graduation Year', name='grad_year', blank=False, choices=YEAR_CHOICES,
                                    max_length=4, default=datetime.datetime.now().year+4
                                    )

    def __str__(self):
        return self.name


class Event(models.Model):
    name = models.CharField('Name', max_length=30, blank=False)
    description = models.TextField('Description', blank=False)
    icon = models.ImageField('Event Image', upload_to='event_icon')
    slug = models.SlugField('Slug', max_length=15)
    students = models.ManyToManyField(Student, verbose_name='Students', related_name='events', blank=True)

    def __str__(self):
        return self.name
