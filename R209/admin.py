from django.contrib import admin
from R209.models import Panel, Event, Student


class PanelAdmin(admin.ModelAdmin):
    list_display = ['identifier', 'name', 'icon', 'description', 'link']


class EventAdmin(admin.ModelAdmin):
    list_display = ['name', 'icon', 'description', 'slug']


class StudentAdmin(admin.ModelAdmin):
    list_display = ['name', 'grad_year']

admin.site.register(Panel, PanelAdmin)
admin.site.register(Event, EventAdmin)
admin.site.register(Student, StudentAdmin)



