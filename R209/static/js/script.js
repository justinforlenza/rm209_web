$('#language-owl').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    stagePadding:60,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:7
        }
    }
});
$('#event-owl').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    stagePadding:60,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:7
        }
    }
});
