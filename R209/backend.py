from random import randrange


def get_random():
    combinations = [
        '123', '124', '132', '134', '142', '143', '214', '213', '234', '231', '243', '241', '312', '314', '321', '324',
        '341', '342', '413', '412', '423', '421', '432', '431'
    ]

    combination = combinations[randrange(0, len(combinations))]

    return combination
