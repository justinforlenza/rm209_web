from django.views.generic.base import TemplateView
from R209.models import Panel
from R209.backend import get_random
from django.shortcuts import render_to_response, redirect
from django.contrib.auth import authenticate, login, logout
from django.template import RequestContext


class Blah(TemplateView):
    template_name = '../templates/blah.html'

    def get_context_data(self, **kwargs):
        context = []
        return context


class Index(TemplateView):
    template_name = '../templates/index.html'

    def get_context_data(self, **kwargs):
        layout = get_random()

        context = {
            'panel1': Panel.objects.all().get(identifier=layout[0]),
            'panel2': Panel.objects.all().get(identifier=layout[1]),
            'panel3': Panel.objects.all().get(identifier=layout[2]),
        }
        return context


def login_user(request):
    username = password = ''
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                redirect('/home/')
    if request.user.is_authenticated():
        return redirect("/")
    else:
        return render_to_response('../templates/login.html', {'username': username}, RequestContext(request))


def logout_user(request):
    if request.user.is_authenticated():
        logout(request)
        return redirect("/")
    else:
        return redirect("/accounts/login/")

