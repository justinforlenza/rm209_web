# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Panel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('identifier', models.CharField(max_length=1)),
                ('name', models.CharField(max_length=10)),
                ('icon', models.CharField(max_length=25)),
                ('description', models.TextField()),
                ('link', models.SlugField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
