# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('R209', '0002_auto_20150811_0041'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name=b'Name')),
                ('description', models.TextField(verbose_name=b'Description')),
                ('icon', models.ImageField(upload_to=b'event_icon', verbose_name=b'Event Image')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
