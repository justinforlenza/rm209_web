# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('R209', '0004_event_slug'),
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name=b'Name')),
                ('grad_year', models.IntegerField(default=2019, max_length=4, verbose_name=b'Graduation Year')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='event',
            name='students',
            field=models.ManyToManyField(related_name='events', verbose_name=b'Students', to='R209.Student', blank=True),
            preserve_default=True,
        ),
    ]
