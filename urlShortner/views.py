from django.http import HttpResponseRedirect, Http404
from urlShortner.models import URL
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from urlShortner.forms import URLForm


def url(request, short):
    try:
        urlObject = URL.objects.get(short_url=short)
        urlObject.update_counter()
        return HttpResponseRedirect(urlObject.original_url)
    except Exception:
        raise Http404


@login_required
def url_create(request):
    if request.method == "POST":
        form = URLForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/url')
    else:
        form = URLForm()
    return render(request, '../templates/short_create.html', {'form': form})