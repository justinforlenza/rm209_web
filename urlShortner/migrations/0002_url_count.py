# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('urlShortner', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='url',
            name='count',
            field=models.IntegerField(default=0, max_length=3, verbose_name=b'Count'),
            preserve_default=True,
        ),
    ]
