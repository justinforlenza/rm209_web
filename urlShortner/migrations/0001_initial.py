# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='URL',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('short_url', models.CharField(unique=True, max_length=6, verbose_name=b'Shortned URL')),
                ('original_url', models.CharField(max_length=200, verbose_name=b'Original Url')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
