from django.db import models


class URL(models.Model):
    short = models.CharField('Shortned URL', max_length=6, name='short_url', unique=True, blank=False)
    original = models.CharField('Original Url',max_length=200, name='original_url', blank=False)
    count = models.IntegerField('Count', max_length=3, name='count', default=0)

    def update_counter(self):
        self.count += 1
        super(URL, self).save()
