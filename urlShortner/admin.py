from django.contrib import admin
from urlShortner.models import URL


class UrlAdmin(admin.ModelAdmin):
    list_display = ['short_url', 'original_url', 'count']

admin.site.register(URL, UrlAdmin)
