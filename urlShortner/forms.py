from django import forms
from urlShortner.models import URL


class URLForm(forms.ModelForm):

    class Meta:
        model = URL

