from django.views.generic import ListView
from R209.models import Event
from showcase.models import ProgramLanguage, Project


class ProjectShowcase(ListView):
    model = Project
    template_name = '../templates/project_showcase.html'
    context_object_name = 'projects'

    def get_queryset(self):
        q = self.request.GET.get('q')

        qs = super(ProjectShowcase, self).get_queryset()

        if q:
            name = qs.filter(name__icontains=q)
            description = qs.filter(description__icontains=q)
            event = qs.filter(event__name__icontains=q)
            language = qs.filter(language__name__icontains=q)
            qs = name | description | event | language

        return qs.order_by('git_link','link', 'source', 'name')

    def get_context_data(self, **kwargs):
        context = super(ProjectShowcase, self).get_context_data(**kwargs)
        context['languages'] = ProgramLanguage.objects.all()
        context['events'] = Event.objects.all()

        return context


class LanguageShowcase(ListView):
    model = Project
    context_object_name = 'projects'
    template_name = '../templates/language_showcase.html'

    def get_queryset(self):
        q = self.request.GET.get('q')

        qs = super(LanguageShowcase, self).get_queryset()
        qs = qs.filter(language__slug=self.kwargs['language'])

        if q:
            name = qs.filter(name__icontains=q)
            description = qs.filter(description__icontains=q)
            event = qs.filter(event__name__icontains=q)
            qs = name | description | event

        return qs

    def get_context_data(self, **kwargs):
        context = super(LanguageShowcase, self).get_context_data(**kwargs)
        context['language'] = ProgramLanguage.objects.get(slug=self.kwargs['language'])
        return context


class EventShowcase(ListView):
    model = Project
    context_object_name = 'projects'
    template_name = '../templates/event_showcase.html'

    def get_queryset(self):
        q = self.request.GET.get('q')

        qs = super(EventShowcase, self).get_queryset()
        qs = qs.filter(event__slug=self.kwargs['event'])

        if q:
            name = qs.filter(name__icontains=q)
            description = qs.filter(description__icontains=q)
            language = qs.filter(language__name__icontains=q)
            qs = name | description | language

        return qs

    def get_context_data(self, **kwargs):
        context = super(EventShowcase, self).get_context_data(**kwargs)
        context['event'] = Event.objects.get(slug=self.kwargs['event'])
        return context
