from django.contrib import admin
from showcase.models import ProgramLanguage, Project


class ProgramLanguageAdmin(admin.ModelAdmin):
    list_display = ['name', 'description']


class ProjectAdmin(admin.ModelAdmin):
    list_display = ['name', 'language', 'event']

admin.site.register(ProgramLanguage, ProgramLanguageAdmin)
admin.site.register(Project, ProjectAdmin)
