# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('showcase', '0005_auto_20150813_0233'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='link',
            field=models.FileField(upload_to=b'', verbose_name=b'Project Link', blank=True),
            preserve_default=True,
        ),
    ]
