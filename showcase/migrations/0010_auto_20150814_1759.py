# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('showcase', '0009_auto_20150814_1753'),
    ]

    operations = [
        migrations.RenameField(
            model_name='project',
            old_name='project_link',
            new_name='link',
        ),
    ]
