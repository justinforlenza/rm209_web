# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('R209', '0005_auto_20150813_0221'),
        ('showcase', '0004_auto_20150812_0011'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='project',
            name='student',
        ),
        migrations.AddField(
            model_name='project',
            name='students',
            field=models.ManyToManyField(related_name='projects', verbose_name=b'Students', to='R209.Student'),
            preserve_default=True,
        ),
    ]
