# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('showcase', '0002_programlanguage_icon'),
    ]

    operations = [
        migrations.AddField(
            model_name='programlanguage',
            name='slug',
            field=models.SlugField(default=b'Blah', verbose_name=b'Slug'),
            preserve_default=True,
        ),
    ]
