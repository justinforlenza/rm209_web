# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('R209', '0004_event_slug'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ProgramLanguage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name=b'Name')),
                ('description', models.TextField(default=b'No Description', verbose_name=b'Description', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=30, verbose_name=b'Name')),
                ('description', models.TextField(verbose_name=b'Description')),
                ('source', models.FileField(upload_to=b'projects/source', verbose_name=b'Source Code Download', blank=True)),
                ('img', models.ImageField(upload_to=b'projects/icons', verbose_name=b'Project Icon')),
                ('event', models.ForeignKey(related_name='project', verbose_name=b'Event', blank=True, to='R209.Event')),
                ('language', models.ForeignKey(related_name='projects', verbose_name=b'Programming Language', to='showcase.ProgramLanguage')),
                ('student', models.ForeignKey(related_name='projects', verbose_name=b'Student', to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
