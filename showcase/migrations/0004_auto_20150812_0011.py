# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('showcase', '0003_programlanguage_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='project',
            name='student',
            field=models.CharField(default=b'None', max_length=140, verbose_name=b'Student(s)', blank=True),
            preserve_default=True,
        ),
    ]
