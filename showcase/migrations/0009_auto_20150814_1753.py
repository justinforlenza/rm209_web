# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('showcase', '0008_auto_20150814_1707'),
    ]

    operations = [
        migrations.RenameField(
            model_name='project',
            old_name='link',
            new_name='project_link',
        ),
        migrations.AddField(
            model_name='project',
            name='git_link',
            field=models.CharField(max_length=140, verbose_name=b'Git Link', blank=True),
            preserve_default=True,
        ),
    ]
