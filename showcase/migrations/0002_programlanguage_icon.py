# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('showcase', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='programlanguage',
            name='icon',
            field=models.ImageField(upload_to=b'languages/icons', verbose_name=b'Icon', blank=True),
            preserve_default=True,
        ),
    ]
