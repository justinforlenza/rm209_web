from django.db import models
from R209.models import Event, Student


class ProgramLanguage(models.Model):
    name = models.CharField('Name', max_length=30, blank=False)
    description = models.TextField('Description', blank=True, default='No Description')
    icon = models.ImageField('Icon', upload_to='languages/icons', blank=True)
    slug = models.SlugField('Slug', blank=False, default='Blah')

    def __str__(self):
        return self.name


class Project(models.Model):
    name = models.CharField('Name', max_length=25, blank=False)
    students = models.ManyToManyField(Student, related_name='projects', verbose_name='Students')
    language = models.ForeignKey(ProgramLanguage, related_name='projects', verbose_name='Programming Language')
    event = models.ForeignKey(Event, related_name='project', verbose_name='Event', blank=True)
    description = models.TextField('Description', blank=False)
    source = models.FileField('Source Code Download', blank=True, upload_to='projects/source')
    git_link = models.CharField('Git Link', blank=True, max_length=140)
    link = models.CharField('Project Link', blank=True, max_length=140)
    img = models.ImageField('Project Icon', blank=False, upload_to='projects/icons')

    def __str__(self):
        return self.name
